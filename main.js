// Assignment 4

// Task1
function charReverseHandler (str) {
    // your code here
    var output = ''
    for (let i=0; i<str.length; i++){
        if (str[i]===str[i].toUpperCase() && str[i]!==str[i].toLowerCase()){
            output += str[i].toLowerCase()
        } else if (str[i]!==str[i].toUpperCase() && str[i]===str[i].toLowerCase()){
            output += str[i].toUpperCase()
        }
        else {
            output += str[i]
        }
    }
    return output
}


var ch = "The Quick Brown"
const rd = ch.split("").reduce(function (result, item){
    if (item.charCodeAt() > 64 && item.charCodeAt() < 91){
        return result+item.toLocaleLowerCase()
    }else if (item.charCodeAt() < 123 && item.charCodeAt() > 96){
        return result+item.toLocaleUpperCase()
    }else{
        return result+item
    }
}, "");

// console.log(charReverseHandler(ch));
// console.log(rd);


// Task 2











// Task 3 
function findDublicatedHandler(arr) {
    return arr.filter((e, i, a) => a.indexOf(e) !== i)
}

// console.log(findDublicatedHandler([1, 8, 8, 2, 1]));



// Task 4

function union (arr1, arr2){
    return arr1.filter(x => arr2.includes(x))
}

// console.log(union([18, -11, 15], [15,-11, 1, 18]));



// Task 5 

function removeEl(arr) {
    return arr.filter(val => !!val);
  }

console.log(removeEl([0, null, 42, undefined, "", true, false, NaN, "", "foo bar"]));



// Task 6
function sort_str(str){
    return str.split('').sort().join('');
}

// console.log(sort_str('webmaster'));